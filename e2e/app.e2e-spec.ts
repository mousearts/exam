import { ExamfePage } from './app.po';

describe('examfe App', () => {
  let page: ExamfePage;

  beforeEach(() => {
    page = new ExamfePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
