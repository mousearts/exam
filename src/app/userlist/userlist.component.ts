//Module
import { Component, OnInit } from '@angular/core';

//Service
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor( private ApiService:ApiService ) { }


  userList: Object[];


  // ngOnInit() {
  //   this.ApiService.getData()
  //   .subscribe(result => this.userList = result);
  // }

  ngOnInit() {
    this.ApiService.getData()
    .subscribe(userList => {console.log(userList);
    this.userList = userList
    })
  }

  deleteUser(index){
    this.userList.splice(index, 1)
  }

}
