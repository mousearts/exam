import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  constructor() { }

  @Input("user") userList : Object [];

  newName : string = "";
  newEmail : string = "";
  newTelephone : string = "";
  newAddress : string = "";
  newCompany : string = "";

  addUser(){
    this.userList.push({"name": this.newName, "email": this.newEmail, "phone": this.newTelephone, "address.street": this.newAddress, "company.name": this.newCompany});
  }

  ngOnInit() {
  }

}
