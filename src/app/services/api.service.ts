//Module
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService {

  constructor(private RequestUser:Http) { 
  }


  getData(){
    return this.RequestUser
    .get('https://jsonplaceholder.typicode.com/users')
    .map(result => result.json() )
    .catch(error => Observable.throw(error.json().error) || "Server Error");
  }

  

}
